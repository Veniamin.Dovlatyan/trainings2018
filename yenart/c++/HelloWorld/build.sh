echo "Pre-processing..."
g++ -E main.cpp -o main.ii || exit 1
echo "Done"

echo "Assembling..."
g++ -S main.ii  -o main.s || exit 2
echo "Done"

echo "Compiling..."
g++ -c main.s   -o main.o || exit 3
echo "Done"

echo "Linking..."
g++    main.o   -o HelloWorld || exit 4
echo "Done"

